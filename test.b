#read the data
setwd("C:/Users/ofirl/OneDrive/שולחן העבודה/לימודים/קורסים/סמסטר ב שנה ג/כרית ידע/data base")
onion<-read.csv('onion.csv',stringsAsFactors = FALSE)
library(ggplot2)
library(tm)
#a reper file
onion.reper<-onion

str(onion)
summary(onion)

#1.2 turnig label to factor and to yes or no
make_yes_or_no <- function(x){
  if(x==0)return('no')
  return('yes')
}
onion$label<-sapply(onion$label,make_yes_or_no)
onion$label<-as.factor(onion$label)
#1.4 pie chart

pie<-summary(onion$label)
pie(pie, main="Yes/No Piechart", col=rainbow(length(pie)),
    labels=c("No","Yes"))

# Q2
#make a corpus

onion.corpus <- Corpus(VectorSource(onion$text))
onion.corpus[[1]][[1]]

#cut coment word in english
onion.corpus <- tm_map(onion.corpus, removeWords, stopwords())
onion.corpus[[1]][[1]]
#cut punctuation
onion.corpus <- tm_map(onion.corpus, removePunctuation)  
onion.corpus[[1]][[1]]
#cut number
onion.corpus <- tm_map(onion.corpus, removeNumbers)
#cut stripWhitespace
onion.corpus<- tm_map(onion.corpus, stripWhitespace)
onion.corpus[[1]][[1]]
# cut big letter
onion.corpus <- tm_map(onion.corpus, content_transformer(tolower)) 
onion.corpus[[1]][[1]]

#2.1
#making DTM data frame
dtm.onion <- DocumentTermMatrix(onion.corpus)
#terms
dim(dtm.onion)
inspect(dtm.onion)

#2.2
freq_function <- function(clean,dtm,n){
  dtm_freq<- DocumentTermMatrix(clean, list(dictionary = findFreqTerms(dtm,n))) 
  return (dtm_freq)
}

#2.3
build.bar <- function(dtm,FUN,clean){
  vec.for.chart <- c()
  v <- seq(100,1000,5)
  for(i in v){
    dtm.freq <- FUN(clean,dtm,i)
    vec.for.chart <- c(vec.for.chart, dim(dtm.freq)[2])
  }
  return(barplot(vec.for.chart))
}
chart<- build.bar(dtm,freq_function,onion.corpus)



#2.4
dtm <- DocumentTermMatrix(onion.corpus)
dtm <- freq_function(onion.corpus,dtm,100)

2.5
conv_01 <- function(x){
x <- ifelse(x>0, 1,0)
x<- as.integer(x)
return(x)
}

dtm.final <- apply(dtm, MARGIN = 1:2, conv_01)
dtm.df.fn <- as.data.frame(dtm.final)

filter <- sample.split(dtm.df.fn, SplitRatio = 0.7)

df.train <- subset(dtm.df.fn, filter == T)
df.test <- subset(dtm.df.fn, filter == F)

dim(dtm.df.fn)[1]
dim(df.train)[1]
dim(df.test)[1]

#Q3
#3.1
library(caTools)
str(df.train)
filter <- sample.split(onion, SplitRatio = 0.7)
df.train <- subset(onion, filter ==T)
df.test <- subset(onion, filter ==F)

model <- glm(label ~ ., family = binomial(link = 'logit'), data = df.train)
predicted.test <- predict(model, newdata = aus.test, type = 'response')
confusion_matrix <- table(predicted.test >0.5,df.test)
confusion_matrix
recall <- confusion_matrix[2,2]/(confusion_matrix[2,2] + confusion_matrix[1,2])
precision <- confusion_matrix[2,2]/(confusion_matrix[2,2] + confusion_matrix[2,1])

#3.2


model2 <- rpart(label~.,df.trian)

predict <- predict(model,df.test)

prediction <- predict> 0.5 
actual <- df.test$label
 
conf_matrix <- table(predicted,actual)
conf_matrix

recall <- conf_matrix[2,2]/(conf_matrix[2,2] + conf_matrix[1,2])
precision <- conf_matrix[2,2]/(conf_matrix[2,2] + conf_matrix[2,1])
recall
precision



